# Individual Research Paper CS7IS5 ADAPTIVE APPLICATIONS
MSc Computer Science - Intelligent Systems<br />
Name: Joshua Cassidy<br />
Student Number: 20300057<br />
Module: CS7IS5 ADAPTIVE APPLICATIONS


# Project Dependencies  (Note the specific version of all the Python dependencies can be found in requirements.txt):
1. Python3 (Note: ensure you have python mapped to the python3 environment variable)
3. NumPy
4. Sci-kit learn (Note: ensure 0.22.1 is the version of Sci-kit learn is being used)
5. Matplotlib (Note: ensure that your operating system is able to use the TKAgg function)
6. pip (Note: ensure you have pip mapped to the pip3 environment variable)
7. pandas

# Install the Projects dependencies
The following steps detail how to install the projects python dependencies:
1. Use the cd command in order to navigate to the directory where the project code has been downloaded
2. Run the "pip3 install -r requirements.txt" command to install the project's python dependencies (Note: ensure that you are connected to the internet and you have python3 and pip installed)

# Running the Project
The following steps detail how to run the supplementary code for the individual research paper:
1. Use the cd command in order to navigate to the directory where the individual research paper has been downloaded
2. Run the "python3 wine_baseline.py" command in order to reproduce the baseline models results that were reported in the research paper
3. Watch the output of the console for program output containing the baseline model results
4. Run the "python3 wine_birch_genetic_algorithms.py" command in order to reproduce the GA BIRCH framework results that were reported in the research paper
5. Watch the output of the console for program output containing the GA BIRCH framework results
6. Open up the plots directory to see the line graph showing the generations against Davies-Bouldin Index
