import matplotlib
matplotlib.use("TkAgg")
import matplotlib.pyplot as plt
import random
import pandas as pd
from sklearn.cluster import Birch
import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
import statistics
from sklearn import metrics

random.seed(20300057)
np.random.seed(20300057)

CROSSOVER_RATE = 0.5
MUTATION_RATE = 0.05
THRESHOLD = len("1111")
CLUSTERS = len("1111")
BRANCH_FACTOR = len("1111111")

class Individual:
    def __init__(self, new_individual, data_directory):
        self.clusters = []
        self.threshold = []
        self.branching_factor = []
        self.fitness = 0
        self.data_directory = data_directory
        self.davies_bouldin = 0
        self.rand = 0
        self.homogeneity = 0
        if new_individual == True:
            self.generate_individual()

    def generate_individual(self):
        for i in range(BRANCH_FACTOR):
            self.branching_factor.append(random.randint(0,1))
        
        for i in range(THRESHOLD):
            self.threshold.append(random.randint(0,1))
        
        for i in range(CLUSTERS):
            self.clusters.append(random.randint(0,1))

        if int("".join(str(i) for i in self.clusters),2) < 2:
            self.clusters[-2] = 1

        if int("".join(str(i) for i in self.clusters),2) > 20:
            self.clusters = [1, 0, 1, 0, 0]

        if int("".join(str(i) for i in self.branching_factor),2) < 2:
            self.branching_factor[-2] = 1

        self.evaluate_individual()

    def evaluate_individual(self):
        scaler = StandardScaler()
        skf = StratifiedKFold(n_splits=10, shuffle=True, random_state=20300057)
        data = pd.read_csv(self.data_directory, delimiter=",", header=None)
        labels = data.iloc[:, 0]
        data = data.iloc[:, 1:]
        
        branching_factor = int("".join(str(i) for i in self.branching_factor),2)
        clusters = int("".join(str(i) for i in self.clusters),2)
        threshold = int("".join(str(i) for i in self.threshold),2) / 10
        davies_bouldin = []
        rand = []
        homogeneity = []
        
        for train, test in skf.split(data, labels):
            train_data = scaler.fit_transform(data.iloc[train])
            test_data = scaler.transform(data.iloc[test])
            birch_model = Birch(n_clusters=clusters, branching_factor=branching_factor, threshold=threshold)
            birch_model.fit(train_data)
            pred = birch_model.predict(test_data)
            davies_bouldin.append(metrics.davies_bouldin_score(test_data, pred))

            train_labels = birch_model.labels_
            mapped_labels = []
            label_counts = {}
            for i in range(len(train_data)):
                d = labels.iloc[i]
                p = train_labels[i]
                if not label_counts.get(str(d) + "-" + str(p)):
                    label_counts[str(d) + "-" + str(p)] = {
                        "actual": d,
                        "train_labels": p,
                        "count": 0,
                    }
                label_counts[str(d) + "-" + str(p)]["count"] += 1
            cluster_mapper = {}
            for k, v in label_counts.items():
                if not cluster_mapper.get(v["train_labels"]):
                    cluster_mapper[v["train_labels"]] = { "actual": v["actual"], "count": v["count"] }
                if cluster_mapper[v["train_labels"]]["count"] < v["count"]:
                    cluster_mapper[v["train_labels"]] = { "actual": v["actual"], "count": v["count"] }
            for i in pred:
                mapped_labels.append(cluster_mapper[i]["actual"])

            rand.append(metrics.adjusted_rand_score(labels[test], mapped_labels))
            homogeneity.append(metrics.homogeneity_score(labels[test], mapped_labels))

        self.fitness = -statistics.mean(davies_bouldin)
        self.davies_bouldin = statistics.mean(davies_bouldin)
        self.rand = statistics.mean(rand)
        self.homogeneity = statistics.mean(homogeneity)

        return self.fitness
    
    def __str__(self):
        return """Individual(
                    fitness={fitness}, 
                    clusters={clusters},
                    threshold={threshold},
                    branching_factor={branching_factor},
                    rand={rand},
                    homogeneity={homogeneity},
                    davies_bouldin={davies_bouldin},
                )""".format(
                    fitness=str(self.fitness),
                    clusters=str(self.clusters),
                    threshold=str(self.threshold),
                    branching_factor=str(self.branching_factor),
                    rand=str(self.rand),
                    homogeneity=str(self.homogeneity),
                    davies_bouldin=str(self.davies_bouldin),
                )

class Population:
    def __init__(self, population_size, new_population, data_directory):
        self.individuals = []
        self.population_size= population_size
        for _ in range(population_size):
            new_individual = Individual(new_individual=new_population, data_directory=data_directory)
            self.individuals.append(new_individual)

    def get_fittest_individual(self):
        fittest_individual = self.individuals[0]
        for current_individual in self.individuals:
            if current_individual.fitness > fittest_individual.fitness:
                fittest_individual = current_individual
        return fittest_individual


class Genetics:
    def __init__(self, data_directory):
        self.data_directory = data_directory

    def evolve_population(self, population):
        next_generation = Population(population.population_size, new_population=False, data_directory=self.data_directory)
        for i in range(population.population_size):
            parent1 = self.select_individual(population)
            parent2 = self.select_individual(population)
            child = self.crossover(parent1, parent2)
            next_generation.individuals[i] = child
        for i in range(population.population_size):
            self.mutate(next_generation.individuals[i])
        return next_generation

    def crossover(self, parent1, parent2):
        child = Individual(new_individual=False, data_directory=self.data_directory)
        for i in range(CLUSTERS):
            if random.uniform(0, 1) < CROSSOVER_RATE:
                child.clusters.append(parent1.clusters[i])
            else:
                child.clusters.append(parent2.clusters[i])
        
        for i in range(THRESHOLD):
            if random.uniform(0, 1) < CROSSOVER_RATE:
                child.threshold.append(parent1.threshold[i])
            else:
                child.threshold.append(parent2.threshold[i])

        for i in range(BRANCH_FACTOR):
            if random.uniform(0, 1) < CROSSOVER_RATE:
                child.branching_factor.append(parent1.branching_factor[i])
            else:
                child.branching_factor.append(parent2.branching_factor[i])
        return child

    def mutate(self, child):
        for i in range(CLUSTERS):
            if random.uniform(0, 1) < MUTATION_RATE:
                if child.clusters[i] == 1:
                    child.clusters[i] = 0
                else:
                    child.clusters[i] = 1

        for i in range(THRESHOLD):
            if random.uniform(0, 1) < MUTATION_RATE:
                if child.threshold[i] == 1:
                    child.threshold[i] = 0
                else:
                    child.threshold[i] = 1

        for i in range(BRANCH_FACTOR):
            if random.uniform(0, 1) < MUTATION_RATE:
                if child.branching_factor[i] == 1:
                    child.branching_factor[i] = 0
                else:
                    child.branching_factor[i] = 1

        if int("".join(str(i) for i in child.clusters),2) > 20:
            child.clusters = [1, 0, 1, 0, 0]

        if int("".join(str(i) for i in child.clusters),2) < 2:
            child.clusters[-2] = 1

        if int("".join(str(i) for i in child.branching_factor),2) < 2:
            child.branching_factor[-2] = 1
        
    
    def select_individual(self, population):
        tournament_size = 7
        fittest_individual = random.choice(population.individuals)

        for _ in range(tournament_size):
            current_fittest_individual = random.choice(population.individuals)
            if current_fittest_individual.fitness > fittest_individual.fitness:
                fittest_individual = current_fittest_individual
        return fittest_individual


population = Population(15, True, "data/wine.csv")
genetics = Genetics("data/wine.csv")
dbis = [population.get_fittest_individual().davies_bouldin]
generation = [1]
for i in range(10):
    population = genetics.evolve_population(population=population)
    for member in population.individuals:
        member.evaluate_individual()
    dbis.append(population.get_fittest_individual().davies_bouldin)
    generation.append(i+2)

plt.rc('font', size=11)
plt.rcParams['figure.constrained_layout.use'] = True

plt.xlabel('Generations')
plt.ylabel('BIRCH Model Davies–Bouldin Index')
plt.title("Visualisation of BIRCH Model Davies–Bouldin Index VS Generations")

plt.plot(generation, dbis)
plt.savefig('plots/BIRCH_Genetic_Algorithms_Convergence.png')

print("BEST INDIVIDUAL")
print(population.get_fittest_individual())
