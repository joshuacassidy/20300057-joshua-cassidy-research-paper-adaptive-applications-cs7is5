import numpy as np
from sklearn.preprocessing import StandardScaler
import pandas as pd
from sklearn import metrics
import statistics

from sklearn.cluster import Birch
from sklearn.model_selection import StratifiedKFold

scaler = StandardScaler()
skf = StratifiedKFold(n_splits=10, shuffle=True, random_state=20300057)
data = pd.read_csv("data/wine.csv", delimiter=",", header=None)
labels = data.iloc[:, 0]
data = data.iloc[:, 1:]

davies_bouldin = []
rand = []
homogeneity = []

for train, test in skf.split(data, labels):
    train_data = scaler.fit_transform(data.iloc[train])
    test_data = scaler.transform(data.iloc[test])
    birch_model = Birch(n_clusters=3)
    birch_model.fit(train_data)
    pred = birch_model.predict(test_data)
    davies_bouldin.append(metrics.davies_bouldin_score(test_data, pred))

    train_labels = birch_model.labels_
    mapped_labels = []
    label_counts = {}
    for i in range(len(train_data)):
        d = labels.iloc[i]
        p = train_labels[i]
        if not label_counts.get(str(d) + "-" + str(p)):
            label_counts[str(d) + "-" + str(p)] = {
                "actual": d,
                "train_labels": p,
                "count": 0,
            }
        label_counts[str(d) + "-" + str(p)]["count"] += 1
    cluster_mapper = {}
    for k, v in label_counts.items():
        if not cluster_mapper.get(v["train_labels"]):
            cluster_mapper[v["train_labels"]] = { "actual": v["actual"], "count": v["count"] }
        if cluster_mapper[v["train_labels"]]["count"] < v["count"]:
            cluster_mapper[v["train_labels"]] = { "actual": v["actual"], "count": v["count"] }
    for i in pred:
        mapped_labels.append(cluster_mapper[i]["actual"])

    rand.append(metrics.adjusted_rand_score(labels[test], mapped_labels))
    homogeneity.append(metrics.homogeneity_score(labels[test], mapped_labels))

fitness = -statistics.mean(davies_bouldin)
rand = statistics.mean(rand)
homogeneity = statistics.mean(homogeneity)

print("baseline davies_bouldin", statistics.mean(davies_bouldin))
print("baseline fitness", fitness)
print("baseline rand", rand)
print("baseline homogeneity", homogeneity)
